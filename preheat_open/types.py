from datetime import datetime
from typing import Optional, Union

TYPE_DATETIME_INPUT = Union[str, datetime]
