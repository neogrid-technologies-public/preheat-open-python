.. Preheat Open documentation master file, created by
   sphinx-quickstart on Thu Oct 14 10:14:21 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. mdinclude:: ../../README.md

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorials/demo.ipynb
   package.rst
   tutorials.rst

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
