# Release notes for preheat_open

## Release 1.39

Features:
- Support of pandas 2.0.0
- Improved comfort profile

Fixes:
- Weekly resampling with pandas is now faster
- Fix to documentation building with Sphinx
- Removing usage of infer_datetime_format
- Fixing graph
- Fixed typo in standard_unit_divisor

Documentation:
- Improved docstrings across the module
- Improved type hints

## Release 1.38

Features:
- HTTP errors now provide more details
- Speed improvement through generalised list-comprehensions
- Warning for already existing data in units has now a dedicated class
- load_data methods now support the usage of enums

Fixes:
- Bad behaviour on query_units with shared units

Development:
- Update of precommits

## Release 1.37

Features:
- Improved robustness of tests
- Grouped all exceptions in exception module
- Adding support of comfort profiles
- Added support of API post
- Building objects can now list their units

Fixes:
- Fixed lexer warning in documentation
- Removed usage of BaseException in exceptions
- Ensuring that exception classnames now end with Error (PEP8)
- Removal of bare except
- In tests, clearing of data has a protection method (not used yet)
- Fixed bad spreading of price data over period

Documentation:
- Improved documentation of Building class

## Release 1.36

From this version onwards, the code is only compatible with Python>=3.9.

Features:
- Support for Enum in timestep

Fixes:
- Removing imports of List, Dict and Tuple from typing (obsolete in recent python)

Documentation:
- Restructuring of the documentation in Sphinx


## Release 1.35

From this version onwards, the code is only compatible with Python>=3.9.

Features:
- New features for devices: querying, loading state

Fixes:
- Fixed warnings from .loc in pandas>=1.5
- Fixed backwards compatibility warnings
- Fixes to the loggers for more robustness

Documentation:
- Integration with Read The Docs
- Removing usage of Gitlab CI/CD
